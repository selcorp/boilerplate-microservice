module.exports = {
  env: {
    es6: true,
    node: true,
    mocha: true,
  },
  extends: ['airbnb-base'],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
    getContextObject: 'readonly',
    expect: 'readonly'
  },
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'script',
    ecmaFeatures: {
      impliedStrict: false,
    },
  },
  rules: {
    strict: ['error', 'global'],
    'no-param-reassign': ['error', { props: false }],
  },
};
