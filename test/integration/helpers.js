'use strict';

const chai = require('chai');
const getContextObject = require('../support/defaulContext');

const mongo = require('../../src/infra/mongoose');

const clearDatabase = require('../support/clearDatabase');

global.expect = chai.expect;
global.getContextObject = getContextObject;

before(() => mongo.connect());

beforeEach(clearDatabase);

after(() => mongo.disconnect());
