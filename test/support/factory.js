'use strict';

const path = require('path');
const { factory, MongooseAdapter } = require('factory-girl');
const { FactoriesLoader } = require('../../src/infra/factoryGirl');

const factoryGirl = new factory.FactoryGirl();
factoryGirl.setAdapter(new MongooseAdapter());

module.exports = FactoriesLoader.load({
  factoryGirl,
  baseFolder: path.join(__dirname, 'factories'),
});
