/* eslint-disable no-console */

'use strict';

const log = (...args) => console.log(...args);
log.error = (...args) => console.error(...args);
log.verbose = (...args) => console.verbose(...args);
log.info = (...args) => console.info(...args);
log.warn = (...args) => console.warn(...args);

module.exports = () => ({
  res: null,
  log,
});
