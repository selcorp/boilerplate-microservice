'use strict';

const fs = require('fs');
const path = require('path');

module.exports = {
  load({ factoryGirl, baseFolder }) {
    fs
      .readdirSync(baseFolder)
      .filter((file) => (file.indexOf('.') !== 0) && (file.slice(-3) === '.js'))
      .forEach((file) => {
        const factoryPath = path.join(baseFolder, file);
        // eslint-disable-next-line
        const factory = require(factoryPath);

        factory(factoryGirl);
      });

    return factoryGirl;
  },
};
