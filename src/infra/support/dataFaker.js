'use strict';

// eslint-disable-next-line import/no-extraneous-dependencies
const Chance = require('chance');

const chance = new Chance();

module.exports = chance;
